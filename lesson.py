class Lesson(object):
    subject = None
    teacher = None
    time = None
    type = None
    auditory = None
    subgroup = None

    def __init__(self, subject, type, auditory, subgroup, time, teacher):
        self.subject = subject
        self.teacher = teacher
        self.time = time
        self.type = type
        self.auditory = auditory
        self.subgroup = subgroup
