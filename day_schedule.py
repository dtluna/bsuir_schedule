class DaySchedule(object):
    lessons = []  # список пар
    date = None
    week_number = None  # 1 - 4
    subgroup = None  # 1 - 2

    def __init__(self, date, week_number, subgroup):
        self.date = date
        self.week_number = week_number
        self.subgroup = subgroup

    def add_lesson(self, lesson):
        self.lessons.append(lesson)
