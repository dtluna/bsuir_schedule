#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from xmlformatter import Formatter
from glob import glob

form = Formatter(encoding_input='UTF-8', encoding_output='UTF-8')
xmls = glob('*.xml')


if __name__ == '__main__':
    for file in xmls:
        print(form.format_file(file).decode("utf-8", "strict"))
