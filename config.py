from xml.etree.ElementTree import parse
from const import all_groups_xml, config_file, subgroup_values


def check_config(config):
    subgroup = config.get('subgroup')
    if subgroup:
        if subgroup not in subgroup_values:
            raise RuntimeError("Corrupted config file '%s'" % (config_file))
    group_name = config.get('group_name')
    if group_name:
        if not group_exists(group_name):
            raise RuntimeError("Corrupted config file '%s'" % (config_file))


def group_exists(group_name):
    doc = parse(all_groups_xml)
    for group in doc.findall('studentGroup'):
        if group.findtext('name') == group_name:
            return True
    return False


def load_config():
    config = {}
    with open(config_file) as f:
        for line in f.readlines():
            key, config[key] = line.strip().split('=')
    print(config)
    check_config(config)
    return config


config = load_config()


def save_config(config):
    with open(config_file, 'w') as f:
        for key in config:
            f.writeline(key + '=' + config[key])
